/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * ClientEnd.cpp
 *
 *  Created on: Nov 20, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#include "ClientEnd.h"
#include "Util.h"
#include "Const-impl.h"
#include <sstream>

namespace iit {
namespace cs550 {
namespace finalproj {

ClientEnd::ClientEnd(string brkhost, int brkport, string endname) :
		_clntEndBrkhost(brkhost), _clntEndBrkport(brkport), _resQueueName() {

	init(endname);
}

ClientEnd::~ClientEnd() {

	_pClntEndSession.close();

	/*	if (_pClntEndSession != NULL) {

	 _pClntEndSession->close();
	 delete _pClntEndSession;
	 _pClntEndSession = NULL;
	 }*/

	if (_pClntEndSubscriptions != NULL) {

		_pClntEndSubscriptions->cancel(_resQueueName);
		delete _pClntEndSubscriptions;
		_pClntEndSubscriptions = NULL;
	}

	if (_pClntEndConnection != NULL) {

		_pClntEndConnection->close();
		delete _pClntEndConnection;
		_pClntEndConnection = NULL;
	}

}

void ClientEnd::received(Message& msg) {

	received_response(msg);
}

void ClientEnd::received_response(const Message& message) {
}

void ClientEnd::listen() {

	listen_response();
}

void ClientEnd::listen_response() {

	/*
	 * Waiting for responses
	 * */
	_pClntEndSubscriptions->run();
	_pClntEndConnection->close();
}

void ClientEnd::async_listen_response() {

	pthread_t tid;
	pthread_create(&tid, NULL, threaded_listen_response, this);
}

void* ClientEnd::threaded_listen_response(void *arg) {

	ClientEnd *pthis = (ClientEnd*) arg;

	/*
	 * Waiting for responses
	 * */
	pthis->_pClntEndSubscriptions->run();
	pthis->_pClntEndConnection->close();
}

void ClientEnd::init(string endname) {

	/*
	 * init connection and session
	 * */
	_pClntEndConnection = new Connection();
	_pClntEndConnection->open(_clntEndBrkhost, _clntEndBrkport);

	_pClntEndSession = _pClntEndConnection->newSession();
	/*_pClntEndSession = new AsyncSession(session);
	 *_pClntEndSession = session;*/

	/*
	 * init response_queue listened by ClientEnd
	 * */
	stringstream ss;
	ss << endname << "-" << LOCALIPADDR << "-"
			<< _pClntEndSession.getId().getName();

	_resQueueName = ss.str();

	//Use name of response_queue as the routing key
	string bindingKey = _resQueueName;

	_pClntEndSession.queueDeclare(arg::queue = _resQueueName, arg::exclusive =
			true, arg::autoDelete = true);
	_pClntEndSession.exchangeBind(arg::exchange = Const::EXCHANGE_DIRECT,
			arg::queue = _resQueueName, arg::bindingKey = bindingKey);

	/*
	 * create a listener and subscribe it to the response_queue
	 * */
	if (PRINT_LOG)
		cout << "Activating response_queue listener for: " << _resQueueName
				<< endl;
	_pClntEndSubscriptions = new SubscriptionManager(_pClntEndSession);
	_pClntEndSubscriptions->subscribe(*this, _resQueueName);
}

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
