/*
 * Copyright 2010-2020 DatasysLab@iit.edu(http://datasys.cs.iit.edu/index.html)
 *      Director: Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of ZHT library(http://datasys.cs.iit.edu/projects/ZHT/index.html).
 *      Tonglin Li(tli13@hawk.iit.edu) with nickname Tony,
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Ke Wang(kwang22@hawk.iit.edu) with nickname KWang,
 *      Dongfang Zhao(dzhao8@@hawk.iit.edu) with nickname DZhao,
 *      Ioan Raicu(iraicu@cs.iit.edu).
 *
 * ConfHandler.h
 *
 *  Created on: Aug 7, 2012
 *      Author: Xiaobingo
 *      Contributor: Tony, KWang, DZhao
 */

#ifndef CONFIGHANDLER_H_
#define CONFIGHANDLER_H_

#include "ConfEntry.h"

#include <map>
#include <vector>
#include <string>
#include <sys/types.h>

#include "Const-impl.h"

using namespace std;

namespace iit {
namespace cs550 {
namespace finalproj {

class ConfHandler {
public:
	typedef map<string, ConfEntry> MAP; //key: PORT,50000 AND value:ConfEntry(PORT,50000)
	typedef pair<string, ConfEntry> PAIR;
	typedef MAP::iterator MIT;
	typedef MAP::reverse_iterator MRIT;

	typedef vector<ConfEntry> VEC;
	typedef VEC::iterator VIT;
	typedef VEC::reverse_iterator VRIT;

	typedef map<string, VEC> NMAP; //key: localhost:50010 AND value:a vector of ConfEntry(localhost:50005)
	typedef pair<string, VEC> NPAIR;
	typedef NMAP::iterator NMIT;
	typedef NMAP::reverse_iterator NMRIT;

public:
	ConfHandler();
	virtual ~ConfHandler();

public:
	static void initConf(string envConf);
	static void print_map();

	static string get_brokerhost();
	static int get_brokerport();
	static int get_tasks_eachpull();
	static bool is_hungry_mode_on();
	static int get_num_worker_threads();
	static int get_tasks_pull_interval();
	static int get_tasks_poll_interval();
	static int get_reservation_poll_interval();

private:
	static void clear_all();
	static void setEnvParameters(const string& envConf);
	static void setParametersInternal(string configFile, MAP& configMap);

	static string get_envconf_parameter(const string &paraname);

public:
	static MAP EnvParameters;

public:
	static string CONF_ENV;

private:
	static bool BEEN_INIT;
};

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* CONFIGHANDLER_H_ */
