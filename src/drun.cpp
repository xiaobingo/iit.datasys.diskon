/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * drun.cpp
 *
 *  Created on: Nov 19, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#include "Frontend.h"
#include "ConfHandler.h"
#include "Util.h"

using namespace iit::cs550::finalproj;

#include  <getopt.h>
#include  <stdlib.h>
#include   <stdio.h>
#include   <string>
#include   <exception>
using namespace std;

void printUsage(char *argv_0);

int main(int argc, char **argv) {

	extern char *optarg;

	int printHelp = 0;
	int nodes = 0;
	int tasksPerNode = 1;
	int log_out = 0;
	string execmd;
	string conf = "frontend.conf";

	int c;
	while ((c = getopt(argc, argv, "N:n:e:c:lh")) != -1) {
		switch (c) {
		case 'N':
			nodes = atoi(optarg);
			break;
		case 'n':
			tasksPerNode = atoi(optarg);
			break;
		case 'e':
			execmd = string(optarg);
			break;
		case 'c':
			conf = string(optarg);
			break;
		case 'l':
			log_out = 1;
			break;
		case 'h':
			printHelp = 1;
			break;
		default:
			fprintf(stderr, "Illegal argument \"%c\"\n", c);
			printUsage(argv[0]);
			exit(1);
		}
	}

	int helpPrinted = 0;
	if (printHelp) {
		printUsage(argv[0]);
		helpPrinted = 1;
		exit(1);
	}

	try {

		if (nodes >= 1 && tasksPerNode >= 1 && !execmd.empty()) {

			ConfHandler::initConf(conf);
			string brokerhost = ConfHandler::get_brokerhost();
			int brokerport = ConfHandler::get_brokerport();

			double start = TimeUtil::getTime_msec();

			Frontend::PRINT_LOG = false;
			Frontend fe(brokerhost, brokerport, nodes, execmd, tasksPerNode);
			fe.listen();

			double end = TimeUtil::getTime_msec();

			double timespan = end - start;

			if (log_out) {

				/*fprintf(stdout, "%s: %f", fe.getJobId().c_str(),
				 timespan / (1000 * 1.0));*/

				fprintf(stdout, "%s: %f\n", fe.getJobId().c_str(), start);
				fprintf(stdout, "%s: %f\n", fe.getJobId().c_str(), end);
				fflush(stdout);
			}

		} else {

			if (!helpPrinted)
				printUsage(argv[0]);
		}
	} catch (exception& e) {

		fprintf(stderr, "%s, exception caught:\n\t%s\n", "main", e.what());
	}

}

void printUsage(char *argv_0) {

	fprintf(stdout, "Usage:\n%s %s\n", argv_0,
			"-N Nodes -n tasksPerNode -e command [-c frontend.conf] [-l] [-h(help)]");
}

