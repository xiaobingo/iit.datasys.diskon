/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * ServerEnd.cpp
 *
 *  Created on: Nov 20, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#include "ServerEnd.h"
#include "Const-impl.h"
#include "Util.h"

#include <cstdlib>
#include <iostream>

#include <sstream>

namespace iit {
namespace cs550 {
namespace finalproj {

ServerEnd::ServerEnd(string brkhost, int brkport, string reqQueueName,
		string bindingKey) :
		_srvrEndBrkhost(brkhost), _srvrEndBrkport(brkport), _reqQueueName(
				reqQueueName), _bindingKey(bindingKey) {

	init();
}

ServerEnd::~ServerEnd() {

	_pSrvrEndSession.close();

	/*	if (_pSrvrEndSession != NULL) {

	 _pSrvrEndSession->close();
	 delete _pSrvrEndSession;
	 _pSrvrEndSession = NULL;
	 }*/

	if (_pSrvrEndSubscriptions != NULL) {

		_pSrvrEndSubscriptions->cancel(_reqQueueName);
		delete _pSrvrEndSubscriptions;
		_pSrvrEndSubscriptions = NULL;
	}

	if (_pSrvrEndConnection != NULL) {

		_pSrvrEndConnection->close();
		delete _pSrvrEndConnection;
		_pSrvrEndConnection = NULL;
	}
}

void ServerEnd::received(Message& msg) {

	received_request(msg);
}

void ServerEnd::received_request(const Message& message) {
}

void ServerEnd::init() {

	/*
	 * init connection and session
	 * */
	_pSrvrEndConnection = new Connection();
	_pSrvrEndConnection->open(_srvrEndBrkhost, _srvrEndBrkport);

	_pSrvrEndSession = _pSrvrEndConnection->newSession();
	/*_pSrvrEndSession = new AsyncSession();
	 *_pSrvrEndSession = session;*/

	/*
	 * init request queue
	 * */
	_pSrvrEndSession.queueDeclare(arg::queue = _reqQueueName, arg::exclusive =
			true, arg::autoDelete = true); //auto delete the queue when the process exits.
	_pSrvrEndSession.exchangeBind(arg::exchange = Const::EXCHANGE_DIRECT,
			arg::queue = _reqQueueName, arg::bindingKey = _bindingKey);

	/*
	 * create a listener and subscribe it to the request_queue
	 * */
	cout << "Activating request_queue listener for: " << _reqQueueName << endl;
	_pSrvrEndSubscriptions = new SubscriptionManager(_pSrvrEndSession);
	_pSrvrEndSubscriptions->subscribe(*this, _reqQueueName);

}

void ServerEnd::listen() {

	listen_request();
}

void ServerEnd::listen_request() {

	/*
	 * Waiting for requests
	 * */
	_pSrvrEndSubscriptions->run();
	_pSrvrEndConnection->close();
}

void ServerEnd::async_listen_request() {

	pthread_t tid;
	pthread_create(&tid, NULL, threaded_listen_request, this);
}

void* ServerEnd::threaded_listen_request(void *arg) {

	ServerEnd *pthis = (ServerEnd*) arg;

	/*
	 * Waiting for requests
	 * */
	pthis->_pSrvrEndSubscriptions->run();
	pthis->_pSrvrEndConnection->close();
}

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
