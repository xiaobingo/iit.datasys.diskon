/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * Const.cpp
 *
 *  Created on: Nov 19, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#include "Const-impl.h"

namespace iit {
namespace cs550 {
namespace finalproj {

const string Const::CONF_DELIMITERS = " ";

const string Const::EXCHANGE_DIRECT = "amq.direct";

const string Const::QUEUE_FOR_DIST_REQUEST = "queue_for_dist_request";
const string Const::QUEUE_FOR_DIST_MONITOR = "queue_for_dist_monitor";

const string Const::REQUEST_FOR_DIST_REQUEST_QUEUE =
		"request_for_dist_request_queue";
const string Const::REQUEST_FOR_DIST_MONITOR = "request_for_dist_monitor";

const string Const::BROKER_HOST = "BROKER_HOST";
const string Const::BROKER_PORT = "BROKER_PORT";

const string Const::TASKS_EACHPULL = "TASKS_EACHPULL";
const string Const::HUNGRY_MODE_ON = "HUNGRY_MODE_ON";
const string Const::NUM_WORKER_THREADS = "NUM_WORKER_THREADS";
const string Const::TASKS_PULL_INTERVAL = "TASKS_PULL_INTERVAL";
const string Const::TASKS_POLL_INTERVAL = "TASKS_POLL_INTERVAL";
const string Const::RESERVATION_POLL_INTERVAL = "RESERVATION_POLL_INTERVAL";

Const::Const() {
}

Const::~Const() {
}

string Const::trim(const string& value) {

	string str = value;
	stringstream trimmer;
	trimmer << str;
	trimmer >> str;

	return str;
}

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
