/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * Const.h
 *
 *  Created on: Nov 19, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#ifndef CONST_H_
#define CONST_H_

#include <iostream>
#include <string>
#include <stdint.h>
using namespace std;

namespace iit {
namespace cs550 {
namespace finalproj {

/*
 *
 */
class Const {
public:
	Const();
	virtual ~Const();

public:
	template<class TYPE> static int toInt(const TYPE& ele);
	template<class TYPE> static uint64_t toUInt64(const TYPE& ele);
	template<class TYPE> static string toString(const TYPE& ele);
	template<class TYPE1, class TYPE2> static string concat(const TYPE1& ele1,
			const TYPE2& ele2);
	template<class TYPE1, class TYPE2> static string concat(const TYPE1& ele1,
			const string& delimiter, const TYPE2& ele2);

	static string trim(const string& value);

	template<class MapType> static
	void print_map(const MapType & map, const string &separator, ostream &os);

public:

	static const string CONF_DELIMITERS; //delimiters used in config file

	/*
	 * EXCHANGE_
	 * */
	static const string EXCHANGE_DIRECT;

	/*
	 * QUEUE_
	 * */
	static const string QUEUE_FOR_DIST_REQUEST;
	static const string QUEUE_FOR_DIST_MONITOR;

	/*
	 * REQUEST_
	 * */
	static const string REQUEST_FOR_DIST_REQUEST_QUEUE;
	static const string REQUEST_FOR_DIST_MONITOR;

	/*
	 * BROKER_
	 * */
	static const string BROKER_HOST;
	static const string BROKER_PORT;

	/*
	 * BACK.CONF
	 * */
	static const string TASKS_EACHPULL;
	static const string HUNGRY_MODE_ON;
	static const string NUM_WORKER_THREADS;
	static const string TASKS_PULL_INTERVAL;
	static const string TASKS_POLL_INTERVAL;
	static const string RESERVATION_POLL_INTERVAL;

};

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* CONST_H_ */
