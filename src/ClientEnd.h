/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * ClientEnd.h
 *
 *  Created on: Nov 20, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#ifndef REQUESTEND_H_
#define REQUESTEND_H_

#include <qpid/client/Connection.h>
#include <qpid/client/Session.h>
#include <qpid/client/AsyncSession.h>
#include <qpid/client/MessageListener.h>
#include <qpid/client/SubscriptionManager.h>

using namespace qpid::client;
using namespace qpid::framing;

#include <string>
using namespace std;

#include "EndPoint.h"

namespace iit {
namespace cs550 {
namespace finalproj {

/*
 *
 */
class ClientEnd: public EndPoint, public MessageListener {
public:
	ClientEnd(string brkhost, int brkport, string endname);
	virtual ~ClientEnd();

public:
	virtual void listen();
	virtual void received(Message& msg);

protected:
	virtual void init(string endname);
	virtual void listen_response();
	virtual void async_listen_response();
	virtual void received_response(const Message& message);

private:
	static void* threaded_listen_response(void *arg);

protected:

	/*
	 * broker info
	 * */
	string _clntEndBrkhost;
	int _clntEndBrkport;

	/*
	 * response_queue info
	 * */
	string _resQueueName;

	/*
	 * other info
	 * */
	AsyncSession _pClntEndSession;
	Connection *_pClntEndConnection;
	SubscriptionManager *_pClntEndSubscriptions;
};

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* REQUESTEND_H_ */
