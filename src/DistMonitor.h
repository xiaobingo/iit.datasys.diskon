/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * DistMonitor.h
 *
 *  Created on: Nov 20, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#ifndef DISTMONITOR_H_
#define DISTMONITOR_H_

#include "ServerEnd.h"
#include "monpack.pb.h"
#include "Const-impl.h"

#include <boost/bimap/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/support/lambda.hpp>
using namespace boost::bimaps;

#include <string>
using namespace std;

namespace iit {
namespace cs550 {
namespace finalproj {

/*
 *
 */
class DistMonitor: public ServerEnd {

public:
	typedef bimap<string, multiset_of<int> > WMAP;
	typedef typename WMAP::left_iterator LIT;
	typedef typename WMAP::left_const_iterator CLIT;
	typedef typename WMAP::value_type Workload;

public:
	DistMonitor(string brkhost, int brkport, string reqQueueName =
			Const::QUEUE_FOR_DIST_MONITOR, string bindingKey =
			Const::REQUEST_FOR_DIST_MONITOR);
	virtual ~DistMonitor();

public:
	virtual void received_request(const Message &message);

private:
	void process_backend_registration(const MonPack &monpack);
	void process_workload_update(const MonPack &monpack);
	void process_backends_probing(const Message &message);

private:
	WMAP _wlMap; //work load map

};

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* DISTMONITOR_H_ */
