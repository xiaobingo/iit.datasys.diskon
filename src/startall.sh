b=$1
s=1
echo "stopping qpid......"
killall -9 qpidd & sleep 3


echo ""
echo "starting qpid......"
~/install/sbin/qpidd & sleep $s


echo ""
echo "starting distributed monitor......"
./distmd -c backend.conf & sleep $s


echo ""
i=1
while [ $i -le $b ]
do 
#echo $i
echo "starting backend $i......"
./backendd -c backend.conf & sleep $s
let "i=i+1"
done


echo ""
echo "starting distributed request queue......"
./distrqd -c backend.conf & sleep $s
