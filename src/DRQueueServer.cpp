/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * DRQueueServer.cpp
 *
 *  Created on: Nov 20, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#include "DRQueueServer.h"
#include "Const-impl.h"
#include "monpack.pb.h"
#include "drqpack.pb.h"

namespace iit {
namespace cs550 {
namespace finalproj {

DRQueueServer::DRQueueServer(string brkhost, int brkport, string reqQueueName,
		string bindingKey) :
		ServerEnd(brkhost, brkport, reqQueueName, bindingKey), ClientEnd(
				brkhost, brkport, "DRQueueServer::ClientEnd") {

}

DRQueueServer::DRQueueServer(string srvrEndBrkhost, int srvrEndBrkport,
		string clntEndBrkhost, int clntEndBrkport, string reqQueueName,
		string bindingKey) :
		ServerEnd(srvrEndBrkhost, srvrEndBrkport, reqQueueName, bindingKey), ClientEnd(
				clntEndBrkhost, clntEndBrkport, "DRQueueServer::ClientEnd") {
}

DRQueueServer::~DRQueueServer() {
}

void DRQueueServer::memorizeJob(const DrqPack &drqpack) {

	/*entable job*/
	entable_job(drqpack);

	/*enqueue tasks for job*/
	enqueue_tasks(drqpack);

}

void DRQueueServer::entable_job(const DrqPack &drqpack) {

	string jobid = drqpack.jobid();

	_jobTable.insert(jobid, "1");
}

void DRQueueServer::enqueue_tasks(const DrqPack &drqpack) {

	/*optional bytes taskid = 1;
	 optional bytes jobid = 2;
	 optional bytes bId = 3;
	 optional bytes replayAddr = 4;
	 optional bytes execmd = 5;
	 optional bytes exeresult = 6;*/

	/*
	 * build tasks from job, put tasks into task queue
	 * */

	int total = drqpack.nodes() * drqpack.taskspernode();
	for (int i = 0; i < total; i++) {

		TaskPack taskpack;
		Uuid uuid(true);
		taskpack.set_taskid(uuid.str());
		taskpack.set_jobid(drqpack.jobid());
		taskpack.set_replayaddr(drqpack.replayaddr());
		taskpack.set_execmd(drqpack.execmd());

		_taskQueue.push(taskpack);
	}
}

void DRQueueServer::probe_backends(const DrqPack &drqpack) {

	/*
	 * probe backends to run tasks from distributed-monitor, todo: async
	 * */

	//MonPack
	//1:backend registration; 2:backend reports status; 3:scheduler probes backends
	/*required int32 mtype = 1;
	 optional bytes bid = 2;
	 optional int32 wqueuelen = 3;
	 repeated bytes bcandidate = 4;
	 optional int32 runtasks = 5;*/

	/*
	 * build backends-probing-pack
	 * */
	MonPack monpack;
	monpack.set_mtype(3);
	//2 * m(m = nodes * taskspernode) backends are probed by scheduler
	int runtasks = 2 * drqpack.nodes() * drqpack.taskspernode();
	monpack.set_runtasks(runtasks);

	/*
	 * build backends-probing-pack and send to distributed_monitor
	 * */
	Message request;

	request.getDeliveryProperties().setRoutingKey(
			Const::REQUEST_FOR_DIST_MONITOR);

	request.getMessageProperties().setReplyTo(
			ReplyTo(Const::EXCHANGE_DIRECT, _resQueueName));

	request.setData(monpack.SerializeAsString());

	_pClntEndSession.messageTransfer(qpid::client::arg::content = request,
			qpid::client::arg::destination = Const::EXCHANGE_DIRECT);
}

void DRQueueServer::process_jobsubmission(const Message& message) {

	DrqPack drqpack;
	drqpack.ParseFromString(message.getData());

	/*memrize job and tasks submitted*/
	memorizeJob(drqpack);

	/*probe backend candidates to run task*/
	probe_backends(drqpack);

}

void DRQueueServer::process_jobFinished(const Message& message) {

	DrqPack drqpack;
	drqpack.ParseFromString(message.getData());

	_jobTable.erase(drqpack.jobid());
}

void DRQueueServer::process_taskspull(const Message& message) {

	DrqPack drqpack;
	drqpack.ParseFromString(message.getData());

	//DrqRequest
	/*required int32 mtype = 1; //1:frontend submits job; 2:frontend reports job finished; 3:backend pulls task
	 optional bytes jobid = 2;
	 optional int32 nodes = 3;
	 optional int32 taskspernode = 4;
	 optional string execmd = 5;
	 optional bytes replayAddr = 6;
	 repeated TaskPack task = 7;
	 repeated int32 pulltasks = 8;*/

	/*while (_taskQueue.empty()) //wait task(s)
	 usleep(5000);*/

	/*
	 * build tasks-pull-pack
	 * */
	DrqPack tasksPullPack;
	tasksPullPack.set_mtype(0); //dummy mtype
	int pulltasks = drqpack.pulltasks();

	int i = 1;

	TaskPack taskPack;
	while (i <= pulltasks && _taskQueue.pop(taskPack)) {

		TaskPack *ptaskPack = tasksPullPack.add_task();
		*ptaskPack = taskPack;
		i++;
	}

	/*
	 * send back tasks-pull-pack to backend
	 * */
	Message response;

	string routingKey =
			message.getMessageProperties().getReplyTo().getRoutingKey();

	response.getDeliveryProperties().setRoutingKey(routingKey); //todo: use ReplyTo property

	response.setData(tasksPullPack.SerializeAsString());

	_pClntEndSession.messageTransfer(qpid::client::arg::content = response,
			qpid::client::arg::destination = Const::EXCHANGE_DIRECT);
}

void DRQueueServer::received_request(const Message& message) {

	//1:frontend submits job; 2:frontend reports job finished; 3:backend pulls task

	DrqPack drqpack;
	drqpack.ParseFromString(message.getData());

	if (drqpack.mtype() == 1) { //1:frontend submits job;

		process_jobsubmission(message);
	} else if (drqpack.mtype() == 2) { //2:frontend reports job finished;

		process_jobFinished(message);
	} else if (drqpack.mtype() == 3) { //3:backend pulls task

		process_taskspull(message);
	} else
		;
}

void DRQueueServer::listen() {

	/*
	 * listen request and response
	 * */
	async_listen_request();

	listen_response();
}

void DRQueueServer::received_response(const Message& message) {

	process_probe_backends_response(message);

}

void DRQueueServer::process_probe_backends_response(const Message& message) {

	/*
	 * get backend candidates to run tasks
	 * */
	MonPack monpack;
	monpack.ParseFromString(message.getData());

	/*
	 * todo: optimize, worker thread to send task-run-reservation
	 * */
	sendTaskRunReservation(monpack);
}

void DRQueueServer::sendTaskRunReservation(const MonPack &monpack) {

	//RsrvPack
	/*optional int32 runtasks = 1;*/

	vector<int> rsvrsPerNode(monpack.bcandidate().size(), 0);

	/*
	 * monpack.runtasks(): means that 2 * m(m = nodes * taskspernode) backends are probed by scheduler
	 *
	 * if (monpack.runtasks() >= monpack.bcandidate().size()) is true
	 *
	 * build reservations for each node in batch
	 * */
	for (int i = 0; i < monpack.runtasks();) {
		for (int j = 0; j < monpack.bcandidate().size(); j++, i++) {

			rsvrsPerNode[j]++;
		}
	}

	/*
	 * send reservations for each node in batch
	 * */
	for (int i = 0; i < rsvrsPerNode.size(); i++) {
		/*
		 * build task-run-reservation-pack
		 * */
		RsrvPack rsrvpack;
		rsrvpack.set_runtasks(rsvrsPerNode[i]);

		/*
		 * build task-run-reservation-request and send to backend
		 * */
		Message request;

		string backendId = monpack.bcandidate(i);

		/*backendId as rounting key, the backend uses its id as name of request_queue*/
		request.getDeliveryProperties().setRoutingKey(backendId);

		request.setData(rsrvpack.SerializeAsString());

		_pClntEndSession.messageTransfer(qpid::client::arg::content = request,
				qpid::client::arg::destination = Const::EXCHANGE_DIRECT);
	}

}

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
