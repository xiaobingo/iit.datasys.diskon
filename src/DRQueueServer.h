/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * DRQueueServer.h
 *
 *  Created on: Nov 20, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#ifndef DRQUEUESERVER_H_
#define DRQUEUESERVER_H_

#include "ServerEnd.h"
#include "ClientEnd.h"

#include  "TSafeQueue-impl.h"
#include  "TSafeMap-impl.h"
#include  "drqpack.pb.h"
#include  "monpack.pb.h"

#include "Const-impl.h"

namespace iit {
namespace cs550 {
namespace finalproj {

/*
 * DRQueueServer
 * as ClientEnd:
 * 1. send reservations to backend
 * 2. talk to distributed_monitor to probe m-lightly-loaded-backends to run tasks
 *
 * as ServerEnd:
 * 1. receive job_submission requests from frontend
 * 2. receive task-pull requests from backend
 */
class DRQueueServer: public virtual ServerEnd, public virtual ClientEnd {
public:
	DRQueueServer(string brkhost, int brkport, string reqQueueName =
			Const::QUEUE_FOR_DIST_REQUEST, string bindingKey =
			Const::REQUEST_FOR_DIST_REQUEST_QUEUE);

	DRQueueServer(string srvrEndBrkhost, int srvrEndBrkport,
			string clntEndBrkhost, int clntEndBrkport, string reqQueueName =
					Const::QUEUE_FOR_DIST_REQUEST, string bindingKey =
					Const::REQUEST_FOR_DIST_REQUEST_QUEUE);

	virtual ~DRQueueServer();

public:
	virtual void listen();

public:
	/*ServerEnd*/
	virtual void received_request(const Message &message);

private:
	/*ServerEnd*/
	void entable_job(const DrqPack &drqpack);
	void enqueue_tasks(const DrqPack &drqpack);
	void memorizeJob(const DrqPack &drqpack);

	void process_jobsubmission(const Message& message);
	void process_jobFinished(const Message& message);
	void process_taskspull(const Message& message);

	void probe_backends(const DrqPack &drqpack);

public:
	/*ClientEnd*/
	virtual void received_response(const Message& message);

private:
	/*ClientEnd*/
	void process_probe_backends_response(const Message& message);
	void sendTaskRunReservation(const MonPack &monpack);

private:
	TSafeMap<string, string> _jobTable;
	TSafeQueue<TaskPack> _taskQueue;
};

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* DRQUEUESERVER_H_ */
