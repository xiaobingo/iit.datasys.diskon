/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * DistMonitor.cpp
 *
 *  Created on: Nov 20, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#include "DistMonitor.h"
#include "monpack.pb.h"

namespace iit {
namespace cs550 {
namespace finalproj {

DistMonitor::DistMonitor(string brkhost, int brkport, string reqQueueName,
		string bindingKey) :
		ServerEnd(brkhost, brkport, reqQueueName, bindingKey) {
}

DistMonitor::~DistMonitor() {
}

void DistMonitor::process_backend_registration(const MonPack &monpack) {

	string backendId = monpack.bid();
	int backendQueueLength = monpack.bqueuelen();
	_wlMap.insert(Workload(backendId, backendQueueLength));
}

void DistMonitor::process_workload_update(const MonPack &monpack) {

	string backendId = monpack.bid();
	int backendQueueLength = monpack.bqueuelen();

	_wlMap.left.modify_data(_wlMap.left.find(backendId), _data =
			backendQueueLength);
}

void DistMonitor::process_backends_probing(const Message &message) {

	MonPack monpack;
	monpack.ParseFromString(message.getData());

	/*
	 * get m-lightly-loaded-backends to run tasks
	 * */
	MonPack resmonpack;
	resmonpack.set_mtype(0); //dummy mtype
	/*
	 * monpack.runtasks(): means that 2 * m(nodes * taskspernode) backends are probed by scheduler
	 * send this-info[2 * m(nodes * taskspernode)] back to scheduler
	 * */
	resmonpack.set_runtasks(monpack.runtasks());

	int i = 1;
	int m = monpack.runtasks();

	for (LIT it = _wlMap.left.begin(); it != _wlMap.left.end(); ++it) {

		//os << it->first << separator << it->second << endl;

		if (i > m)
			break;

		resmonpack.add_bcandidate(it->first);
		i++;
	}

	/*
	 * send back m-lightly-loaded-backends to scheduler
	 * */
	//string routingKey = Const::REQUEST_FOR_DIST_REQUEST_QUEUE;
	string routingKey =
			message.getMessageProperties().getReplyTo().getRoutingKey();

	Message response;

	response.getDeliveryProperties().setRoutingKey(routingKey);

	response.setData(resmonpack.SerializeAsString());

	_pSrvrEndSession.messageTransfer(qpid::client::arg::content = response, qpid::client::arg::destination =
			Const::EXCHANGE_DIRECT);
}

void DistMonitor::received_request(const Message& message) {

	MonPack monpack;
	monpack.ParseFromString(message.getData());

	if (monpack.mtype() == 1) { //backend registration

		process_backend_registration(monpack);

	} else if (monpack.mtype() == 2) { //backend reports status, e.g. queue length

		process_workload_update(monpack);

	} else if (monpack.mtype() == 3) { //scheduler probes backends, e.g. get m lightly-loaded backends to run tasks

		process_backends_probing(message);

	} else
		;

}

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
