/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * Frontend.h
 *
 *  Created on: Nov 19, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#ifndef FRONTEND_H_
#define FRONTEND_H_

#include "ClientEnd.h"
#include "drqpack.pb.h"

namespace iit {
namespace cs550 {
namespace finalproj {

/*
 *
 */
class Frontend: public ClientEnd {
public:
	Frontend(string brkhost, int brkport, int nodes, string execmd,
			int tasksPerNode = 1);
	virtual ~Frontend();

protected:
	/*ClientEnd*/
	virtual void listen_response();
	virtual void received_response(const Message& message);

private:
	/*ClientEnd*/
	void proces_task_finished_response(const TaskPack taskpack);
	void memorizeTaskStatus(const TaskPack &taskpack);
	void finishingJob();
	void reportJobFinished();

public:
	virtual void submitjob();

	virtual string getJobId();

private:

	/*
	 * user submission info
	 * */
	int _nodes;
	string _execmd;
	int _tasksPerNode;

	/*
	 * job info and tasks done
	 * */
	DrqPack _drqpack;
	int _tasksDone;
};

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* FRONTEND_H_ */
