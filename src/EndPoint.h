/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * EndPoint.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#ifndef ENDPOINT_H_
#define ENDPOINT_H_

#include <string>
using namespace std;

#include <qpid/client/Message.h>

using namespace qpid::client;

namespace iit {
namespace cs550 {
namespace finalproj {

/*
 *
 */
class EndPoint {
public:
	EndPoint();
	virtual ~EndPoint();

public:
	virtual void listen() = 0;
	virtual void received(Message& msg) = 0;

protected:
	static string getlocalIpAddr();

public:
	static bool PRINT_LOG;

protected:
	/*
	 * id info
	 * */
	static string LOCALIPADDR;
};

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* ENDPOINT_H_ */

