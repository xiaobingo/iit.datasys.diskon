/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * Frontend.cpp
 *
 *  Created on: Nov 19, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#include "Frontend.h"
#include "Const-impl.h"

#include "drqpack.pb.h"
#include <qpid/types/Uuid.h>

namespace iit {
namespace cs550 {
namespace finalproj {

Frontend::Frontend(string brkhost, int brkport, int nodes, string execmd,
		int tasksPerNode) :
		ClientEnd(brkhost, brkport, "Frontend::ClientEnd"), _nodes(nodes), _execmd(
				execmd), _tasksPerNode(tasksPerNode), _drqpack(), _tasksDone(0) {
}

Frontend::~Frontend() {
}

void Frontend::memorizeTaskStatus(const TaskPack &taskpack) {

	_tasksDone =
			taskpack.jobid() == _drqpack.jobid() ? ++_tasksDone : _tasksDone;

}

void Frontend::finishingJob() {

	/*
	 * check if tasks are finished
	 * */
	if (_tasksDone == _nodes * _tasksPerNode) {

		reportJobFinished();

		_pClntEndSubscriptions->cancel(_resQueueName);
	}
}

void Frontend::reportJobFinished() {

	/*
	 * build job-finished-pack
	 * */
	DrqPack drqpack;
	drqpack.set_mtype(2);
	drqpack.set_jobid(_drqpack.jobid());

	/*
	 * build job-finished-request and send to distributed_request_queue
	 * */
	Message request;

	request.getDeliveryProperties().setRoutingKey(
			Const::REQUEST_FOR_DIST_REQUEST_QUEUE);

	request.setData(drqpack.SerializeAsString());

	_pClntEndSession.messageTransfer(arg::content = request, arg::destination =
			Const::EXCHANGE_DIRECT);

}

void Frontend::received_response(const Message& message) {

	/*
	 * memorize tasks finished
	 * */
	TaskPack taskpack;
	taskpack.ParseFromString(message.getData());

	proces_task_finished_response(taskpack);
}

void Frontend::proces_task_finished_response(const TaskPack taskpack) {

	/*memorize the task finished*/
	memorizeTaskStatus(taskpack);

	finishingJob();
}

void Frontend::listen_response() {

	/*
	 * submit job
	 * */
	submitjob();

	/*
	 * Waiting for job-finished responses
	 * */
	ClientEnd::listen_response();
}

void Frontend::submitjob() {

	/*required int32 mtype = 1; //1:frontend submits job; 2:backend pulls task
	 optional bytes jobid = 2;
	 optional int32 nodes = 3;
	 optional int32 taskspernode = 4;
	 optional string execmd = 5;
	 optional bytes replayAddr = 6;
	 repeated TaskPack task = 7;*/

	/*
	 * build job-submission-pack
	 * */
	Uuid uuid(true);
	_drqpack.set_mtype(1);
	_drqpack.set_jobid(uuid.str());
	_drqpack.set_nodes(_nodes);
	_drqpack.set_execmd(_execmd);
	_drqpack.set_taskspernode(_tasksPerNode);
	_drqpack.set_replayaddr(_resQueueName);

	/*
	 * build job-submission-request and send to distributed_request_queue
	 * */
	Message request;

	string routingKey = _resQueueName;

	request.getMessageProperties().setReplyTo(
			ReplyTo(Const::EXCHANGE_DIRECT, routingKey));

	request.getDeliveryProperties().setRoutingKey(
			Const::REQUEST_FOR_DIST_REQUEST_QUEUE);

	request.setData(_drqpack.SerializeAsString());

	_pClntEndSession.messageTransfer(arg::content = request, arg::destination =
			Const::EXCHANGE_DIRECT);

}

string Frontend::getJobId() {

	return _drqpack.jobid();
}

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
