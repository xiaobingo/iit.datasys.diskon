/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * Const-impl.h
 *
 *  Created on: Nov 19, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#ifndef CONST_IMPL_H_
#define CONST_IMPL_H_

#include "Const.h"

#include <stdlib.h>
#include <sstream>
#include <stdio.h>
#include <iostream>

namespace iit {
namespace cs550 {
namespace finalproj {

template<class TYPE> int Const::toInt(const TYPE& ele) {

	return atoi(toString(ele).c_str());
}

template<class TYPE> uint64_t Const::toUInt64(const TYPE& ele) {

	return strtoull(toString(ele).c_str(), NULL, 10);
}

template<class TYPE> string Const::toString(const TYPE& ele) {

	stringstream ss;
	ss << ele;

	return ss.str();
}

template<class TYPE1, class TYPE2> string Const::concat(const TYPE1& ele1,
		const TYPE2& ele2) {

	stringstream ss;
	ss << ele1;
	ss << ele2;

	return ss.str();
}

template<class TYPE1, class TYPE2> string Const::concat(const TYPE1& ele1,
		const string& delimiter, const TYPE2& ele2) {

	stringstream ss;
	ss << ele1;
	ss << delimiter;
	ss << ele2;

	return ss.str();
}

template<class MapType>
void Const::print_map(const MapType & map, const string & separator,
		ostream & os) {

	typedef typename MapType::const_iterator const_iterator;

	for (const_iterator i = map.begin(), iend = map.end(); i != iend; ++i) {
		os << i->first << separator << i->second << endl;
	}
}

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* CONST_IMPL_H_ */
