/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * ServerEnd.h
 *
 *  Created on: Nov 20, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#ifndef RESPONSEEND_H_
#define RESPONSEEND_H_

#include <qpid/client/Connection.h>
#include <qpid/client/Session.h>
#include <qpid/client/AsyncSession.h>
#include <qpid/client/MessageListener.h>
#include <qpid/client/SubscriptionManager.h>

#include <string>

#include "EndPoint.h"

using namespace qpid::client;
using namespace qpid::framing;

using namespace std;

namespace iit {
namespace cs550 {
namespace finalproj {

/*
 *
 */
class ServerEnd: public EndPoint, public MessageListener {
public:
	ServerEnd(string brkhost, int brkport, string reqQueueName,
			string bindingKey);
	virtual ~ServerEnd();

public:
	virtual void listen();
	virtual void received(Message& msg);

protected:
	virtual void init();
	virtual void listen_request();
	virtual void async_listen_request();
	virtual void received_request(const Message &message);

private:
	static void* threaded_listen_request(void *arg);

protected:

	/*
	 * broker info
	 * */
	string _srvrEndBrkhost;
	int _srvrEndBrkport;

	/*
	 * distributed_request_queue info
	 * */
	string _reqQueueName;
	string _bindingKey;

	/*
	 * other info
	 * */
	AsyncSession _pSrvrEndSession;
	Connection *_pSrvrEndConnection;
	SubscriptionManager *_pSrvrEndSubscriptions;

};

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* RESPONSEEND_H_ */
