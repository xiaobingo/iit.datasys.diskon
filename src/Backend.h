/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * Backend.h
 *
 *  Created on: Nov 21, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#ifndef BACKEND_H_
#define BACKEND_H_

#include "ServerEnd.h"
#include "ClientEnd.h"
#include "TSafeQueue-impl.h"

#include "drqpack.pb.h"

namespace iit {
namespace cs550 {
namespace finalproj {

/*
 * Backend
 * as ClientEnd:
 * 1. register itself to distriuted_monitor
 * 2. report its workload(e.g. queue length) to distriuted-_monitor
 * 3. pull tasks from distributed_request_queue
 *
 * as ServerEnd:
 * 1. accept tasks-run-reservation from scheduler
 */
class Backend: virtual public ServerEnd, virtual public ClientEnd {
public:
public:
	Backend(string brkhost, int brkport, string reqQueueName = ENDID,
			string bindingKey = ENDID);

	Backend(string srvrEndBrkhost, int srvrEndBrkport, string clntEndBrkhost,
			int clntEndBrkport, string reqQueueName = ENDID, string bindingKey =
					ENDID);

	virtual ~Backend();

public:
	virtual void listen();

public:
	/*ServerEnd*/
	virtual void received_request(const Message& message);

private:
	/*ServerEnd*/
	void process_taskRunReservation(const RsrvPack &rsrvpack);
	void enqueueTaskRunReservation(const RsrvPack &rsrvpack);

protected:
	/*ClientEnd*/
	virtual void received_response(const Message& message);

private:
	/*ClientEnd*/
	void process_taskspull_reponse(const DrqPack &drqpack);
	void registerBackEndToMonitor();
	//to optimize: threaded-reportStatusToMonitor(Backend *thisbackend = NULL);
	void reportStatusToMonitor();

	void runTasks();
	static void* threadedRunTasks(void *arg);
	static void runIndividualTask(const TaskPack &taskpack,
			AsyncSession &clntEndSession);

	void pullTasksFromDistReqQueue();
	static void* threadedPullTasksFromDistReqQueue(void *arg);
	void memorizeTasksPull(const DrqPack &drqpack);

private:
	void initme();
	static string genEndId(); //

private:
	/*
	 * id info
	 * */
	static string ENDID;

	/*
	 * back.conf
	 * */
	static int TASKS_EACHPULL;
	static bool HUNGRY_MODE_ON;
	static int NUM_WORKER_THREADS;
	static int TASKS_PULL_INTERVAL;
	static int TASKS_POLL_INTERVAL;
	static int RESERVATION_POLL_INTERVAL;

	/*
	 * worker threads
	 * */
	pthread_t **workers;

	/*
	 * task-to-be-run-queue
	 * */
	static TSafeQueue<TaskPack> TASK_QUEUE;

	/*
	 * task-run-reservation queue
	 * */
	//static TSafeQueue<RsrvPack> RESERVATION_QUEUE;
	static TSafeQueue<int> RESERVATION_QUEUE;

};

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
#endif /* BACKEND_H_ */
