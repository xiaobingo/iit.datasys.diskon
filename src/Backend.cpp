/*
 * Copyright 2013-2020
 *      Advisor: Zhiling Lan(lan@iit.edu), Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of iit::cs550::finalproj(Diskon: Distributed tasK executiON framework)
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Hao Chen(hchen71@hawk.iit.edu) with nickname Hchen,
 *		Iman Sadooghi(isadoogh@iit.edu) with nickname Iman,
 *		Ke Wang(kwang22@hawk.iit.edu) with nickname KWang.
 *
 * Backend.cpp
 *
 *  Created on: Nov 21, 2013
 *      Author: Xiaobingo, Hchen
 *      Contributor: Iman, KWang
 */

#include "Backend.h"
#include "Util.h"
#include "monpack.pb.h"
#include "drqpack.pb.h"
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#include "ConfHandler.h"

namespace iit {
namespace cs550 {
namespace finalproj {

string Backend::ENDID = genEndId();

int Backend::TASKS_EACHPULL = 0;
bool Backend::HUNGRY_MODE_ON = false;
int Backend::NUM_WORKER_THREADS = 0;
int Backend::TASKS_PULL_INTERVAL = 0;
int Backend::TASKS_POLL_INTERVAL = 0;
int Backend::RESERVATION_POLL_INTERVAL = 0;

TSafeQueue<TaskPack> Backend::TASK_QUEUE;

//TSafeQueue<RsrvPack> Backend::RESERVATION_QUEUE;
TSafeQueue<int> Backend::RESERVATION_QUEUE;

Backend::Backend(string brkhost, int brkport, string reqQueueName,
		string bindingKey) :
		ServerEnd(brkhost, brkport, reqQueueName, bindingKey), ClientEnd(
				brkhost, brkport, "Backend::ClientEnd") {
	initme();
}

Backend::Backend(string srvrEndBrkhost, int srvrEndBrkport,
		string clntEndBrkhost, int clntEndBrkport, string reqQueueName,
		string bindingKey) :
		ServerEnd(srvrEndBrkhost, srvrEndBrkport, reqQueueName, bindingKey), ClientEnd(
				clntEndBrkhost, clntEndBrkport, "Backend::ClientEnd") {
	initme();
}

void Backend::initme() {

	TASKS_EACHPULL = ConfHandler::get_tasks_eachpull();
	HUNGRY_MODE_ON = ConfHandler::is_hungry_mode_on();
	NUM_WORKER_THREADS = ConfHandler::get_num_worker_threads();
	TASKS_PULL_INTERVAL = ConfHandler::get_tasks_pull_interval();
	TASKS_POLL_INTERVAL = ConfHandler::get_tasks_poll_interval();
	RESERVATION_POLL_INTERVAL = ConfHandler::get_reservation_poll_interval();

	workers = (pthread_t**) calloc(NUM_WORKER_THREADS, sizeof(pthread_t*));
	for (int i = 0; i < NUM_WORKER_THREADS; i++) {
		workers[i] = (pthread_t*) calloc(1, sizeof(pthread_t));
	}

	printf("tasks_eachpull: %d\n", TASKS_EACHPULL);
	printf("hungry_mode_on: %d\n", HUNGRY_MODE_ON);
	printf("num_worker_threads: %d\n", NUM_WORKER_THREADS);
	printf("tasks_pull_interval: %d(us)\n", TASKS_PULL_INTERVAL);
	printf("tasks_poll_interval: %d(us)\n", TASKS_POLL_INTERVAL);
	printf("reservation_poll_interval: %d(us)\n", RESERVATION_POLL_INTERVAL);
}

Backend::~Backend() {

	free(workers);
}

void Backend::listen() {

	/*
	 *  register backend(itself) to distributed_monitor
	 * */
	registerBackEndToMonitor();

	pullTasksFromDistReqQueue();

	runTasks();

	/*
	 * listen request and response
	 * */
	async_listen_request();

	listen_response();
}

void Backend::received_request(const Message& message) {

	RsrvPack rsrvpack;
	rsrvpack.ParseFromString(message.getData());

	process_taskRunReservation(rsrvpack);
}

void Backend::process_taskRunReservation(const RsrvPack &rsrvpack) {

	/*
	 * enqueue task-run-reservation
	 * */
	enqueueTaskRunReservation(rsrvpack);
}

void Backend::enqueueTaskRunReservation(const RsrvPack &rsrvpack) {

	/*
	 * scheduler sent multiple resverations in batch,
	 * put them one by one into RESERVATION_QUEUE
	 * */
	for (int i = 0; i < rsrvpack.runtasks(); i++) {

		/*RsrvPack reservation;
		 reservation.ParseFromString(rsrvpack.SerializeAsString());*/

		RESERVATION_QUEUE.push(1);
	}
}

void Backend::received_response(const Message& message) {

	DrqPack drqpack;
	drqpack.ParseFromString(message.getData());

	process_taskspull_reponse(drqpack);
}

void Backend::process_taskspull_reponse(const DrqPack &drqpack) {

	/*memorize tasks-pull result*/
	memorizeTasksPull(drqpack);

	/*report backend status(e.g. task-queue-length)*/
	reportStatusToMonitor();
}

void Backend::memorizeTasksPull(const DrqPack &drqpack) {

	/*
	 * dequeue_task-run-reservation queue
	 * enqueue task-to-be-run
	 * */

	int tasks_pulled = drqpack.task().size();

	if (tasks_pulled == 0) {
		RESERVATION_QUEUE.pop_all();
		return;
	}

	int i;
	for (i = 0; i < tasks_pulled; i++) {

		TASK_QUEUE.push(drqpack.task(i));
	}

	RESERVATION_QUEUE.pop_n(tasks_pulled);
}

void Backend::registerBackEndToMonitor() {

	/*
	 * build backend-registration-pack
	 * */
	MonPack monpack;
	monpack.set_mtype(1);
	monpack.set_bid(ENDID);
	monpack.set_bqueuelen(0);

	/*
	 * build backend-registration-request and send to distributed_monitor
	 * */
	Message request;

	request.getDeliveryProperties().setRoutingKey(
			Const::REQUEST_FOR_DIST_MONITOR);

	request.setData(monpack.SerializeAsString());

	_pClntEndSession.messageTransfer(qpid::client::arg::content = request,
			qpid::client::arg::destination = Const::EXCHANGE_DIRECT);
}

void Backend::reportStatusToMonitor() {

	/*
	 * build backend-report-status-pack
	 * */
	MonPack monpack;
	monpack.set_mtype(2);
	monpack.set_bid(ENDID);
	monpack.set_bqueuelen(TASK_QUEUE.size());

	/*
	 * build backend-report-status-request and send to distributed_monitor
	 * */
	Message request;

	request.getDeliveryProperties().setRoutingKey(
			Const::REQUEST_FOR_DIST_MONITOR);

	request.setData(monpack.SerializeAsString());

	_pClntEndSession.messageTransfer(qpid::client::arg::content = request,
			qpid::client::arg::destination = Const::EXCHANGE_DIRECT);

	/*thisbackend == NULL ?
	 _pClntEndSession.messageTransfer(qpid::client::arg::content = request,
	 qpid::client::arg::destination = Const::EXCHANGE_DIRECT) :
	 thisbackend->_pClntEndSession.messageTransfer(qpid::client::arg::content =
	 request, qpid::client::arg::destination = Const::EXCHANGE_DIRECT);*/
}

void Backend::runTasks() {

	/*
	 * start worker thread to run tasks
	 * */

	pthread_t tid;
	for (int i = 0; i < 1; i++) {

		pthread_create(workers[i], NULL, threadedRunTasks, this);
	}
}

/*
 * to optimize, report status to monitor after running tasks
 * */
void* Backend::threadedRunTasks(void *arg) {

	int count = 0;

	Backend *pthis = (Backend*) arg;
	AsyncSession clntEndSession = (pthis->_pClntEndSession);

	while (true) { //looping tasks-run

		TaskPack taskpack;
		bool popped = TASK_QUEUE.pop(taskpack);

		if (!popped) {
			usleep(TASKS_POLL_INTERVAL); //to optimize: interval
			continue;
		}

		runIndividualTask(taskpack, clntEndSession);

		/*TaskPack taskpack;
		 TASK_QUEUE.pop(taskpack);
		 runIndividualTask(taskpack, clntEndSession);*/

		count++;

		/*printf("TASK_QUEUE::size() = {%lu : %d}\n", TASK_QUEUE.size(), count);
		 fflush(stdout);*/

		/*if (count % TASKS_EACHPULL == 0)
		 reportStatusToMonitor(this);*/
	}
}

void Backend::runIndividualTask(const TaskPack &taskpack,
		AsyncSession &clntEndSession) {

	//run individual task

#ifdef HOMO
	std::system(taskpack.execmd().c_str());
#endif

#ifdef HETERO
	srand(time(0));
	int ms = rand() % 128;
	ms++;
	usleep(ms * 1000);
#endif

	/*
	 * build task-run-pack
	 * */
	TaskPack resTaskPack;
	resTaskPack.ParseFromString(taskpack.SerializeAsString());
	resTaskPack.set_exeresult("OK");

	/*
	 * build task-run-response and send to frontend(e.g. identified by TaskPack.replayaddr())
	 * */
	Message request;

	request.getDeliveryProperties().setRoutingKey(resTaskPack.replayaddr());

	request.setData(resTaskPack.SerializeAsString());

	clntEndSession.messageTransfer(qpid::client::arg::content = request,
			qpid::client::arg::destination = Const::EXCHANGE_DIRECT);
}

void Backend::pullTasksFromDistReqQueue() {

	/*
	 * start worker thread to pull tasks from distributed_request_queue
	 * */
	pthread_t tid;
	pthread_create(&tid, NULL, threadedPullTasksFromDistReqQueue, this);
}

/*
 * pull TASKS_EACHPULL tasks each time from distributed_request_queue
 * */
void* Backend::threadedPullTasksFromDistReqQueue(void *arg) {

	Backend *pthis = (Backend*) arg;
	AsyncSession clntEndSession = (pthis->_pClntEndSession);

	int count = 1;
	while (true) { //looping tasks-pull

		int reservations;
		if ((reservations = RESERVATION_QUEUE.size()) == 0) {
			usleep(RESERVATION_POLL_INTERVAL); //to optimize: interval
			continue;
		}

		/*printf("RESERVATION_QUEUE::size() = {%lu}\n", RESERVATION_QUEUE.size());
		 fflush(stdout);*/

		int pulltasks =
				HUNGRY_MODE_ON ? reservations :
				reservations >= TASKS_EACHPULL ? TASKS_EACHPULL : reservations;
		/*
		 * build tasks-pull-pack
		 * */
		DrqPack drqpack;
		drqpack.set_mtype(3);
		drqpack.set_pulltasks(pulltasks);

		/*
		 * build tasks-pull-request and send to distributed_request_queue
		 * */
		Message request;

		request.getDeliveryProperties().setRoutingKey(
				Const::REQUEST_FOR_DIST_REQUEST_QUEUE);

		request.getMessageProperties().setReplyTo(
				ReplyTo(Const::EXCHANGE_DIRECT, pthis->_resQueueName));

		request.setData(drqpack.SerializeAsString());

		clntEndSession.messageTransfer(qpid::client::arg::content = request,
				qpid::client::arg::destination = Const::EXCHANGE_DIRECT);

		count += 2;
		int time = count * TASKS_PULL_INTERVAL;
		usleep(time); //to optimize: interval, throttle-frequent-pull-tasks
	}
}

string Backend::genEndId() {

	/*
	 * local-ip-addr + "-" + uuid, as short-backend-id.
	 * "Backend::ServerEnd" + "-" + short-backend-id, as long-backend-id
	 * this id is different every time when backend is launched
	 * */
	Uuid uuid(true);

	string endid = "Backend::ServerEnd-" + NetUtil::getlocalIpAddr() + "-"
			+ uuid.str();

	return endid;
}

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
