// Boost.Bimap
//
// Copyright (c) 2006-2007 Matias Capeletto
//
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

//  VC++ 8.0 warns on usage of certain Standard Library and API functions that
//  can be cause buffer overruns or other possible security issues if misused.
//  See http://msdn.microsoft.com/msdnmag/issues/05/05/SafeCandC/default.aspx
//  But the wording of the warning is misleading and unsettling, there are no
//  portable alternative functions, and VC++ 8.0's own libraries use the
//  functions in question. So turn off the warnings.
#define _CRT_SECURE_NO_DEPRECATE
#define _SCL_SECURE_NO_DEPRECATE

// Boost.Bimap Example
//-----------------------------------------------------------------------------

#include <string>
#include <iostream>

#include <boost/bimap/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/support/lambda.hpp>

using namespace boost::bimaps;

template<class MapType>
void print_map(const MapType & map, const std::string & separator,
		std::ostream & os) {
	typedef typename MapType::const_iterator const_iterator;

	for (const_iterator i = map.begin(), iend = map.end(); i != iend; ++i) {
		os << i->first << separator << i->second << std::endl;
	}
}

int main2() {
	//[ code_bimap_and_boost_lambda

	typedef bimap<std::string, multiset_of<int> > bm_type;
	//typedef bimap<std::string, int> bm_type;

	bm_type bm;
	bm.insert(bm_type::value_type("one", 1));
	bm.insert(bm_type::value_type("two", 2));

	bm.right.range(5 < _key, _key < 10);

	print_map(bm.left, "-->", std::cout);

	bm.left.modify_data(bm.left.find("one"), _data = 2);

	std::cout << "----------" << std::endl;

	print_map(bm.left, "-->", std::cout);

	bm.left.modify_data(bm.left.begin(), _data *= 10);
	//]
	return 0;
}

#include <stdio.h>
#include <unistd.h>
#include <string.h> /* for strncpy */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <string>
using namespace std;

int main() {
	int fd;
	struct ifreq ifr;

	fd = socket(AF_INET, SOCK_DGRAM, 0);

	/* I want to get an IPv4 IP address */
	ifr.ifr_addr.sa_family = AF_INET;

	/* I want IP address attached to "eth0" */
	strncpy(ifr.ifr_name, "lo", IFNAMSIZ - 1);

	ioctl(fd, SIOCGIFADDR, &ifr);

	close(fd);

	/* display result */
	string ipaddr = inet_ntoa(((struct sockaddr_in *) &ifr.ifr_addr)->sin_addr);
	printf("%s\n", ipaddr.c_str());
	//printf("%s\n", inet_ntoa(((struct sockaddr_in *) &ifr.ifr_addr)->sin_addr));

	return 0;
}

