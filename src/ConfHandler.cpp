/*
 * Copyright 2010-2020 DatasysLab@iit.edu(http://datasys.cs.iit.edu/index.html)
 *      Director: Ioan Raicu(iraicu@cs.iit.edu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of ZHT library(http://datasys.cs.iit.edu/projects/ZHT/index.html).
 *      Tonglin Li(tli13@hawk.iit.edu) with nickname Tony,
 *      Xiaobing Zhou(xzhou40@hawk.iit.edu) with nickname Xiaobingo,
 *      Ke Wang(kwang22@hawk.iit.edu) with nickname KWang,
 *      Dongfang Zhao(dzhao8@@hawk.iit.edu) with nickname DZhao,
 *      Ioan Raicu(iraicu@cs.iit.edu).
 *
 * ConfHandler.cpp
 *
 *  Created on: Aug 7, 2012
 *      Author: Xiaobingo
 *      Contributor: Tony, KWang, DZhao
 */

#include "ConfHandler.h"
#include "ConfEntry.h"
#include "StrTokenizer.h"

#include <stdlib.h>
#include <string>
#include <string.h>
#include <stdio.h>

#include <fstream>
#include <iostream>
using namespace std;

namespace iit {
namespace cs550 {
namespace finalproj {

bool ConfHandler::BEEN_INIT = false;
string ConfHandler::CONF_ENV = "env.conf";

ConfHandler::MAP ConfHandler::EnvParameters = MAP();

ConfHandler::ConfHandler() {

}

ConfHandler::~ConfHandler() {

}

string ConfHandler::get_envconf_parameter(const string &paraname) {

	string result;
	ConfHandler::MAP *zpmap = &ConfHandler::EnvParameters;

	ConfHandler::MIT it;

	for (it = zpmap->begin(); it != zpmap->end(); it++) {

		ConfEntry ce;
		ce.assign(it->first);

		if (ce.name() == paraname) {

			result = ce.value();

			break;
		}
	}

	return result;
}

void ConfHandler::initConf(string envConf) {

	if (!BEEN_INIT) {

		ConfHandler::CONF_ENV = envConf; //env.conf
		ConfHandler::setEnvParameters(envConf);

		BEEN_INIT = true;
	}
}

void ConfHandler::clear_all() {

	EnvParameters.clear();
}

void ConfHandler::setEnvParameters(const string& zhtConfig) {

	setParametersInternal(zhtConfig, EnvParameters);
}

void ConfHandler::setParametersInternal(string configFile, MAP& configMap) {

	ifstream ifs(configFile.c_str(), ifstream::in);

	const char *delimiter = Const::CONF_DELIMITERS.c_str();

	string line;
	while (getline(ifs, line)) {

		string remains = line;

		if (remains.empty())
			continue;

		if (remains.substr(0, 1) == "#") //starts with #, means comment
			continue;

		StrTokenizer strtok(remains);

		string one;
		string two;

		if (strtok.has_more_tokens())
			one = strtok.next_token();

		if (strtok.has_more_tokens())
			two = strtok.next_token();

		if (one.empty())
			continue;

		ConfEntry ce(one, two);
		configMap.insert(PAIR(ce.toString(), ce)); //todo: use hash code to reduce size of key/value pair.
		//configMap[ce.toString()] = ce; //todo: use hash code to reduce size of key/value pair.
	}

	ifs.close();
}

string ConfHandler::get_brokerhost() {

	return get_envconf_parameter(Const::BROKER_HOST);
}

int ConfHandler::get_brokerport() {

	return atoi(get_envconf_parameter(Const::BROKER_PORT).c_str());
}

int ConfHandler::get_tasks_eachpull() {

	return atoi(get_envconf_parameter(Const::TASKS_EACHPULL).c_str());
}

bool ConfHandler::is_hungry_mode_on() {

	int on = atoi(get_envconf_parameter(Const::HUNGRY_MODE_ON).c_str());

	return on == 1;
}

int ConfHandler::get_num_worker_threads() {

	return atoi(get_envconf_parameter(Const::NUM_WORKER_THREADS).c_str());
}

int ConfHandler::get_tasks_pull_interval() {

	return atoi(get_envconf_parameter(Const::TASKS_PULL_INTERVAL).c_str());
}

int ConfHandler::get_tasks_poll_interval() {

	return atoi(get_envconf_parameter(Const::TASKS_POLL_INTERVAL).c_str());
}

int ConfHandler::get_reservation_poll_interval() {
	return atoi(get_envconf_parameter(Const::RESERVATION_POLL_INTERVAL).c_str());
}

void ConfHandler::print_map() {

	/*	NMIT mit;
	 for (mit = NeighborMap.begin(); mit != NeighborMap.end(); mit++) {

	 fprintf(stdout, "<%s>:\n", mit->first.c_str());

	 VEC neighbors = NeighborMap[mit->first];

	 VIT vit;
	 //for (vit = mit->second.begin(); vit != mit->second.end(); vit++) {
	 for (vit = neighbors.begin(); vit != neighbors.end(); vit++) {

	 fprintf(stdout, "\t%s\n", vit->toString().c_str());
	 }

	 fprintf(stdout, "\n");
	 }

	 fflush(stdout);*/
}

} /* namespace finalproj */
} /* namespace cs550 */
} /* namespace iit */
